**_@adra-network/semantic-release-preset_**

###ADRA Official Semantic Versioning Preset

#### Installation

`npm install --save-dev @adra-network/semantic-release-preset `

#### Usage

In your `package.json`:

```json
{
  "release": {
    "extends": "@adra-network/semantic-release-preset"
  }
}
```

#### Contributing

Contributions to the @adra-network/semantic-release-preset are welcome. Please ensure to follow our contributing guidelines and code of conduct.

#### License

This project is licensed under the ISC License - see the LICENSE file for details.

Feel free to adjust the README to include more specific details about your project, such as additional configuration options, advanced usage examples, or how to contribute.
