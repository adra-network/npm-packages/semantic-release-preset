export default {
  branches: ["main"],
  plugins: ["@semantic-release/gitlab", "@semantic-release/commit-analyzer"],
  preset: "conventionalcommits",
};
