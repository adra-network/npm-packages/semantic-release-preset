#!/usr/bin/env node

import fs from "fs/promises";
import path from "path";
import readline from "readline";

// Create a readline interface
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Path to the .releaserc.mjs file
const filePath = path.join(process.cwd(), ".releaserc.mjs");

// File content to be written
const content = `/**
 * @type {import('semantic-release').GlobalConfig}
 */
export default {
  extends: "@adra-network/semantic-release-preset",
};
`;

// Function to check if the file exists and act accordingly
async function checkAndCreateFile() {
  try {
    // Check if the file exists
    await fs.access(filePath);
    // If exists, prompt the user
    rl.question(
      ".releaserc.mjs already exists. Do you want to overwrite it? (yes/no) ",
      async (answer) => {
        if (answer.toLowerCase() === "yes" || answer.toLowerCase() === "y") {
          await fs.writeFile(filePath, content);
          console.log(
            ".releaserc.mjs has been overwritten with the new content."
          );
        } else {
          console.log("Operation cancelled. The file was not overwritten.");
        }
        rl.close();
      }
    );
  } catch (err) {
    // If the file does not exist, create it
    if (err.code === "ENOENT") {
      await fs.writeFile(filePath, content);
      console.log(".releaserc.mjs has been created.");
      rl.close();
    } else {
      // Log other errors
      console.error("An error occurred:", err);
      rl.close();
    }
  }
}

// Run the function
checkAndCreateFile();
