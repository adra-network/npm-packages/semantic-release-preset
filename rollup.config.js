import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import shebang from "rollup-plugin-preserve-shebang";
import terser from "@rollup/plugin-terser";
terser();
const commonPlugins = [nodeResolve(), commonjs(), json(), terser()];
export default [
  {
    input: "src/cli.js",
    output: {
      file: "dist/cli.min.mjs",
      format: "es",
      sourcemap: true,
    },
    plugins: [shebang(), ...commonPlugins],
  },
  {
    input: "src/config.js", // Adjust this path to where your ES6 script is located
    output: [
      {
        file: "dist/.releaserc.min.cjs",
        format: "cjs",
      },
      {
        file: "dist/.releaserc.min.js",
        format: "iife",
        name: "releaserc", // Required for IIFE format. Name of the global variable to hold the module's exports.
      },
      {
        file: "dist/.releaserc.min.mjs",
        format: "es",
      },
    ],
    plugins: [[...commonPlugins]],
  },
];
